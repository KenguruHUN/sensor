import math
import ustruct as struct
from micropython import const

REG_ID = 0xD0
REG_RESET = 0xE0
REG_STATUS = 0xF3
REG_CTRL = 0xF4
REG_CONF = 0xF5

REG_PRESS_MSB = 0xF7
REG_PRESS_LSB = 0xF8
REG_PRESS_XLSB = 0xF9

REG_TEMP_MSB = 0xFA
REG_TEMP_LSB = 0xFB
REG_TEMP_XLSB = 0xFC

SLEEP_MODE = 0x00
NORMAL_MODE_1 = 0x01
NORMAL_MODE_2 = 0x10
FORCED_MODE = 0x11

ULTRA_LOW_POWER = 0

STANDARD_RESOLUTION = 1
HIGH_RESOLUTION = 2
ULTRA_HIGH_RESOLUTION = 3


class BMP280:

    def __init__(self, i2c, addr):
        self._i2c = i2c
        self._addr = addr
        pass
