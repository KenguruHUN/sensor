import socket


class Client:
    
    def __init__(self, is_connected, ip, message=False):
        if is_connected:
            self.own_ip = ip[0]

        if message:
            ADDR = ('255.255.255.255', 60606)
            UDP_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            if UDP_sock.sendto(self.own_ip, ADDR):
                print('#7 Broadcast message sent')
        else:
            print('7# Broadcast message is suppressed')
