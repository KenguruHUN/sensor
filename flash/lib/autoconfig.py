import time
import _thread
from network import WLAN
from machine import I2C

import bmp280
import tsl2591
import vis_helpers as vhp

class InitWiFi:
    
    def __init__(self, cfg):
        self.cfg = cfg
        if self.cfg is None:
            self.wlan = WLAN(mode=WLAN.AP)
        else:
            print('2# Initialize WLAN')
            self.wlan = WLAN(mode=WLAN.STA)
            self.stations = self.wlan.scan()
            print('3# Scan Finished')
        
    def auto_connect(self):
        """Connect to preferred WIFI in STA mode, fallback to AP mode"""
        available_stations = [item for item in self.stations if item.ssid in self.cfg.ACCEPTED_STATIONS]
        for station in available_stations:
            print('4# Station found in config')
            try:
                print('5# Connecting')
                self.wlan.connect(station[0], auth=(WLAN.WPA2, str(self.cfg.PASSWORDS[station[0]])), timeout=5000)
                vhp.flash_two(0x001000)
                break                    
            except Exception as e:
                print('Connection Error')
                vhp.flash_two(0x100000)
                self.cfg = None
                
        if self.cfg is None:
            self.wlan.init(mode=WLAN.AP, 
                    ssid='wipy-wlan', 
                    auth=(WLAN.WPA2, 'www.wipy.io'), 
                    channel=7, 
                    antenna=WLAN.INT_ANT)
    @property
    def is_connected(self): 
        return self.wlan.isconnected()
    
    @property
    def ip(self):
       return self.wlan.ifconfig() 
    

class InitSensors:

    def __init__(self, cfg):
        self.cfg = cfg
        if cfg is None:
            print('Config is not found')
        else:
            print('#8 Initialize sensors')
            i2c = I2C(0, I2C.MASTER)

        sensors = i2c.scan()

        for sensor in sensors:
            if sensor in self.cfg.KNOWN_SENSORS:
                print(self.cfg.KNOWN_SENSORS[sensor]['name'])

            else:
                print('Unidentified sensor detected. Address: {}'.format(sensor))

        # self.bme = bmp280.BMP280(i2c=i2c, addr=)
        self.tsl = tsl2591.TSL2591(i2c=i2c)

        while True:
            print(self.tsl.full_spectrum)
            print(self.tsl.raw_luminosity)
            print(self.tsl.infrared)
            print(self.tsl.visible)
            print(self.tsl.lux)
            time.sleep(3)

