import pycom
import utime

def flash_two(signal):
    pycom.heartbeat(False)
    for _ in range(0, 2):
        pycom.rgbled(signal)
        utime.sleep(0.1)
        pycom.rgbled(0x000000)
        utime.sleep(0.1)

    pycom.heartbeat(True)
