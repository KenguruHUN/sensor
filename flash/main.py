# -*- coding: utf-8 -*-
import time
from machine import SD

import client
from autoconfig import InitWiFi
from autoconfig import InitSensors

try:
    import config as cfg
except ImportError:
    print('Import Error')
    cfg = None
except Exception as e:
    print('Config Error')
    cfg = None

try:
    sd = SD()
except Exception as e:
    print('SD error')
    print(e)

new_wlan = InitWiFi(cfg)
new_wlan.auto_connect()

start_time = time.time()

while not new_wlan.is_connected:
    print('   Waiting for the connection')
    time.sleep(1)
else:
    print('6# Connection established')
    
print("   Wlan is fully up in: %s seconds" % (time.time() - start_time))

new_client = client.Client(new_wlan.is_connected, new_wlan.ip)

sensors = InitSensors(cfg)
