# Sensor

![](docs/source/_static/img/sensor.png)


### What is this ?
This is a simple sensor pack powered by wipy 2.0. It measure the environmental pressure, temeprature and lux.

### Why WiPy ?
Because I love all form of python.

### Future plans

Later this will be a member of a sensor network that measures temperature, lux and air pressure at several points of the apartment.
